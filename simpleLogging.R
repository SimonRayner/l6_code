#!/usr/bin/env Rscript
###############################################################################
# Sample logging
###############################################################################
library(logging)
library(ggplot2)
library(readr)
library(dplR)
library(tools) # for manipulating file names


dataFile<-"/Users/simonray/Dropbox/teaching2018/Lecture2_programming/logging/wool.csv"
logFile = paste(file_path_sans_ext(dataFile),".log", sep="")

###############################################################################
suppressPackageStartupMessages(require(optparse))

option_list <- list(
  make_option(c("-d", "--data"),  action="store", default=NA, type='character', 
              help="data input file"))


opts <- parse_args(OptionParser(option_list=option_list))
dataFile <- ""

if(!is.na(opts$data)){
  dataFile<-opts$data
} else {
  message("you need to specify a input data file")
  logerror("you need to specify a input data file")
  stop("exit")
}
###############################################################################

basicConfig(level='DEBUG')
addHandler(writeToFile, level='DEBUG', file=logFile)
loginfo("logger loaded")
loginfo("Running 'simpleLogging.R'")

loginfo(paste("data file is <", dataFile, ">"))
woolData<-read_csv(dataFile, col_names = TRUE)
loginfo(paste("read ", nrow(woolData), " rows"))

ggplot(woolData, aes(x=time, y=value)) + 
  geom_point(shape=18, color="blue") +
  geom_smooth(color="darkred", method = 'loess', formula = 'y ~ x')

plotFile = paste(file_path_sans_ext(dataFile),".pdf", sep="")
loginfo(paste("plot file is <", plotFile, ">"))
ggsave(plotFile)


# But what happens when we make a change and resave the file? 
# Attach a 'unique' ID to the filename
# now we can cross reference the plot file and the log file to see what
# did to the data to generate the plot

ug <- uuid.gen()

plotFile = paste(file_path_sans_ext(dataFile), ".", ug(), ".pdf", sep="")
loginfo(paste("plot file is <", plotFile, ">"))
ggsave(plotFile)
