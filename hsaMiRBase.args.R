#!/usr/bin/env Rscript
library(logging)

suppressPackageStartupMessages(require(optparse))
suppressPackageStartupMessages(require(tools))


option_list <- list(
  make_option(c("-f", "--fasta"),  action="store", default=NA, type='character', 
              help="fasta input file"),
  make_option(c("-r", "--report"), action="store", default=NA, type='character',
              help="report file name"),
  make_option(c("-l", "--logfile"), action="store", default=NA, type='character',
              help="report file name"),
  make_option(c("-d", "--details"), action="store_false", dest="details", default=TRUE, type='character',
              help="list detailed help")

)

opts <- parse_args(OptionParser(option_list=option_list))
fastaFile <- ""
reportFile <-""
logFile <- ""


if(opts$details==FALSE){
  message("A program to calculate average GC content of in a FASTA file.")
  message("you need to specify a FASTA input file using the -f/--fasta option.")
  message("you may also specify a report file using the -r/--report option, ")
  message("otherwise the report filename will be based on the FASTA filename ")
  message("and written to the same folder.")
  stop("exit")
} 



if(!is.na(opts$fasta)){
  fastaFile<-opts$fasta
} else {
  loginfo("you need to specify a fasta input file")
  stop("exit")
}
paste("Fasta file is <", fastaFile, ">")

if(!is.na(opts$report)){
  reportFile<-opts$report
} else {
  reportFile <- paste(file_path_sans_ext(fastaFile),".GCReport", sep="")
}

basicConfig(level='DEBUG')
addHandler(writeToFile, level='DEBUG', file=logFile)
loginfo("logger loaded")
loginfo("Running 'hsaMiRBase.R'")


loginfo("FASTA file is", fastaFile )
loginfo("Result file is", reportFile)
loginfo("log file is", logFile)

#load data.table package for manipulating data frames
miRBaseFA<-file.path(fastaFile)
miRBaseFA

if(!require(data.table)){
  install.packages("data.table")
  library(data.table)
}

# Biostrings version
if(!require(Biostrings)){
install.packages("Biostrings")
library(Biostrings)
}

allMiRsB = readRNAStringSet(miRBaseFA)
humanMiRsB<-allMiRsB[names(allMiRsB) %like% "hsa"]
af<-alphabetFrequency(humanMiRsB, baseOnly=TRUE, as.prob=TRUE)
bsGCratio<-(colSums(af)/nrow(af))["C"] + (colSums(af)/nrow(af))["G"]



#Seqinr version
if(!require(seqinr)){
install.packages("seqinr")
library(seqinr)
}

allMiRsS <- read.fasta(file = miRBaseFA)
humansMiRsS<-allMiRsS[names(allMiRsS) %like% "hsa"]
gc <- function(x){
GC(x)
}

seqinrGCratio<-mean(sapply(humansMiRsS, gc))

cat("seqinR GC ratio",seqinrGCratio, sep="\t") 
cat("\n")
cat("biostrings GC ratio",bsGCratio, sep="\t")
cat("---")


loginfo("FASTA file is", fastaFile )
loginfo("Result file is", reportFile)

fileRep<-file(reportFile)
  writeLines(c(paste("seqinR GC ratio",seqinrGCratio, sep=":"), 
             paste("biostrings GC ratio",bsGCratio, sep=":")), fileRep)
loginfo("done")