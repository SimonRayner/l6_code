from Bio import SeqIO
from Bio.SeqUtils import GC

# Simple example to calculate GC percentage of human miRNAs in 
# most recent miRBase release (22)
miRBaseAll="/Users/simonrayner/Dropbox/teaching2019/L6_programming/L6_data/mature.fa"
gcPercent = 0.0
hsaCount = 0
for seqRecord in SeqIO.parse(miRBaseAll, "fasta"):

    if "hsa" in seqRecord.id:
        gcPercent = gcPercent + GC(seqRecord.seq)
#        print (str(seqRecord.seq) + "\t" + str(GC(seqRecord.seq)))
        hsaCount += 1

  
gcPercent = gcPercent/hsaCount
print ("GC content is " + str(gcPercent) + "%")
