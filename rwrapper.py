import sys, getopt
import uuid
import Bio

"""sample a subset of n sequences from a supplied fasta file"""
def main(argv):
    inputfile = ''
    rscript = ''
    logfile = ''
    n=1

    u = uuid.uuid1()


    try:
        opts, args = getopt.getopt(argv,"hi:r:",["ifile=","rscript="])
    except getopt.GetoptError:
        print ('rwrapper.py -i <inputfile> -r <rscript>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('rwrapper.py -i <inputfile> -r <rscript>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-r", "--rscript"):
            rscript = arg


            n = arg
    print ('input file is "', inputfile)
    print ('rscript file is "', rscript)
    print ('logfile file is "', logfile)

 
    from Bio import SeqIO
    from random import sample
    from shutil import copyfile
    import os



    print(os.path.dirname(os.path.abspath(rscript)))
    print(str(u) + ".R")
    destFile = os.path.join(os.path.dirname(os.path.abspath(rscript)), str(u) + ".R")
    reportFile = inputfile + "_" + str(u) + ".report"
    print(destFile)
    copyfile(rscript, destFile)
    cmdString = "Rscript " + rscript + " -f " + inputfile + " -r "   +  reportFile + " -l " + str(u)
    print(cmdString)

    #subprocess.call ("/usr/bin/Rscript " + cmdString, shell=True)


    print("done")

if __name__ == "__main__":
   main(sys.argv[1:])